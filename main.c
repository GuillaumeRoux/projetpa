#include "import.h"
#include "world.h"

int main(void)
{
        int i = 0;
        //Allocation de la mémoire
        world_t *world = malloc(sizeof(world_t));
        
        //Définition de certaines variables importantes
        world->vitesse_fond = 3;
        world->pos_map = 0;
        world->i = 0;
        world->k = 1; 
        world->ausol = true;
        world->moveR = false;
        world->moveL = false;
        world->terminer = false;
        world->gravite = GRAVITE;

        
        // Déclaration de la fenêtre
        SDL_Window* fenetre;
        
        // Événements liés à la fenêtre
        SDL_Event evenements; 
        
        // Initialisation de la SDL
        if(SDL_Init(SDL_INIT_VIDEO) < 0) 
        {
            printf("Erreur d’informatitialisation de la SDL: %s",SDL_GetError());
            SDL_Quit();
            return EXIT_FAILURE;
        }
        
        // Créer la fenêtre
        fenetre = SDL_CreateWindow("Fenetre SDL", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE);
        
        // En cas d’erreur
        if(fenetre == NULL) 
        {
            printf("Erreur de la creation d’une fenetre: %s",SDL_GetError());
            SDL_Quit();
            return EXIT_FAILURE;
            
        }
        
        // Mettre en place un contexte de rendu de l’écran
        world->ecran = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_PRESENTVSYNC); 

        world->mob = malloc(sizeof(SDL_Rect *) * 5);
        for(int k = 0; k < 5; k++)
            world->mob[k] = malloc(sizeof(SDL_Rect) * 2);
        world->pos_m = malloc(sizeof(int *) * 5);
        for(int k = 0; k < 5; k++)
            world->pos_m[k] = malloc(sizeof(int) * 2);
        // Appel à la procédure qui initialise tous les SDL_Rect qui seront utilisés
        position_initial(world);

        world->mobT = charger_image("Fonds/enemies.bmp", world->ecran);

        // Chargement du background
        world->fond = charger_image( "Fonds/1-1.bmp", world->ecran );
        
        // Chargement du gameover
        world->gameover = charger_image("Fonds/gameover2.bmp", world->ecran);     

        // Chargement du calque
        world->calque = charger_surface("Fonds/calque.bmp");
               
        // Chargement du sprite
        world->mario = charger_image("Fonds/mario-3.bmp", world->ecran);

        //Chargement de l'écran de win
        world->win = charger_image("Fonds/win2.bmp", world->ecran);
        
        
        // Boucle principale
        while(!world->terminer)	
        {           
            SDL_SetRenderDrawColor(world->ecran,255,255,255,255);

            SDL_RenderClear(world->ecran);
            
            // Affichage sur l'ecran du background et du mario
            SDL_RenderCopy(world->ecran, world->fond, &world->SrcR, &world->DestR);
            SDL_RenderCopy(world->ecran, world->mario, &world->marioS, &world->marioD);
            for (int h = 0; h < 5; h++)
                SDL_RenderCopy(world->ecran, world->mobT, &world->mob[h][0], &world->mob[h][1]);

            // Application de la gravité en continu
            if (world->ausol)
            {
                world->marioD.y += world->gravite;
            }

            // Appel à la procédure qui gère les événements du clavier
            if ( SDL_PollEvent( &evenements ))
            {
                make_event(evenements, world);
            }
                
            // Appel à la procédure qui gère le saut du personnage 
            if (!world->ausol)
            {
                saut(world);
            }
            // Appel à la procédure qui gère le déplacement et les animations du personnage
            move(world);
            
            pos_mob(world);
            if (i % 3 == 0)
                move_mob(world);
            // Appel aux procédures qui gèrent les collisions
            check_collision(world);
            collision(world);
            
            i++;
            // Mise à jour de l'écran
            SDL_RenderPresent(world->ecran);
        }

        // Gestion de fichier ( Crédits )
        FILE* fichier = NULL;
        int CharAct = 0;
        
        fichier = fopen("Crédits.txt", "r");
        
        if (fichier != NULL)
        {
            // Boucle qui lit les caractères un par un
            do
            {
                CharAct = fgetc(fichier); // Lecture du caractère
                printf("%c", CharAct);    // Affichage du caractère
            }while(CharAct != EOF);       // On continue la boucle tant que fgetc ne renvoie pas EOF ( End Of File = Fin Du Fichier)
            
            fclose(fichier);              // Fermeture du fichier
        }
        
        // Libération de l’écran (renderer)
        SDL_DestroyRenderer(world->ecran);
        
        // Quitter SDL  
        SDL_DestroyWindow(fenetre);
        SDL_Quit();
        
        // Libération de la mémoire allouée pour la structure
        free(world);
        
        return 0;
    
}

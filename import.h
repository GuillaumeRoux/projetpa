#include "fonctions_SDL.h"
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define MARIO_HEIGHT 31         //Hauteur du sprite
#define MARIO_WIDTH 20          //Largeur du sprite
#define GRAVITE 3               //Variable de la gravité
#define WINDOW_HEIGHT 280       //Hauteur de la fenêtre de jeu
#define WINDOW_WIDTH 600        //Largeur de la fenêtre de jeu
#define DIFF_COLLISION 16       //Difference entre le y du SDL_Rect du sprite du Mario et le haut du chapeau du Mario
#define POS_X 100               //Position en x du Mario sur la map
#define ALPHA 255               //Valeur du canal alpha quand c'est opaque


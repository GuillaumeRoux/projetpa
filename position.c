#include "world.h"

void pos_mob(world_t *world)
{
    for(int i = 0; i < 5; i++)
        world->mob[i][1].x = world->pos_m[i][0] - world->pos_map;
}

void position_mob(world_t *world)
{
    SDL_Rect **mob = world->mob;

    world->pos_m[0][0] = 352;
    world->pos_m[0][1] = -1;
    world->pos_m[1][0] = 1248;
    world->pos_m[1][1] = -1;
    world->pos_m[2][0] = 1680;
    world->pos_m[2][1] = -1;
    world->pos_m[3][0] = 1952;
    world->pos_m[3][1] = -1;
    world->pos_m[4][0] = 2736;
    world->pos_m[4][1] = -1;

    mob[0][0].x = 22;
    mob[0][0].y = 173;
    mob[0][0].w = 18;
    mob[0][0].h = 18;

    mob[0][1].x = 352;
    mob[0][1].y = 191;
    mob[0][1].w = 18;
    mob[0][1].h = 18;

    mob[1][0].x = 22;
    mob[1][0].y = 173;
    mob[1][0].w = 18;
    mob[1][0].h = 18;

    mob[1][1].x = 1248;
    mob[1][1].y = 191;
    mob[1][1].w = 18;
    mob[1][1].h = 18;

    mob[2][0].x = 3;
    mob[2][0].y = 65;
    mob[2][0].w = 18;
    mob[2][0].h = 26;

    mob[2][1].x = 1680;
    mob[2][1].y = 183;
    mob[2][1].w = 18;
    mob[2][1].h = 26;
    
    mob[3][0].x = 3;
    mob[3][0].y = 65;
    mob[3][0].w = 18;
    mob[3][0].h = 26;

    mob[3][1].x = 1952;
    mob[3][1].y = 183;
    mob[3][1].w = 18;
    mob[3][1].h = 26;
    
    mob[4][0].x = 3;
    mob[4][0].y = 65;
    mob[4][0].w = 18;
    mob[4][0].h = 26;

    mob[4][1].x = 2736;
    mob[4][1].y = 183;
    mob[4][1].w = 18;
    mob[4][1].h = 26;
}

// Procédure qui initialise tous les SDL_Rect qui seront utilisés
void position_initial(world_t *world)
{
    world->SrcR.x = 0;
    world->SrcR.y = 0;
    world->SrcR.w = 600;
    world->SrcR.h = 280;
    world->DestR.x = 0;
    world->DestR.y = 0;
    world->DestR.w = 600;
    world->DestR.h = 280;
    world->marioS.x = 3;
    world->marioS.y = 77;
    world->marioS.w = MARIO_WIDTH;
    world->marioS.h = MARIO_HEIGHT;
    world->marioD.x = 100;
    world->marioD.y = 177;
    world->marioD.w = MARIO_WIDTH;
    world->marioD.h = MARIO_HEIGHT;
    world->marioE.x = 3;
    world->marioE.y = 161;
    world->marioE.w = MARIO_WIDTH;
    world->marioE.h = MARIO_HEIGHT;
    position_mob(world);
}

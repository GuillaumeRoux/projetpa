#include "import.h"
#include <SDL2/SDL.h>
#include "world.h"

void move_mob(world_t *world)
{
    for (int i = 0; i < 5; i++) {
        if ((world->pos_m[i][0] - (world->pos_map+100) <= 200 && (world->pos_m[i][0] - (world->pos_map+100)) >= 0)) {
            world->pos_m[i][0] -= 1;
            if (i > 1)
                world->mob[i][0].x = 3;
        }
        else if (((world->pos_map+100) - world->pos_m[i][0]) >= 0 && ((world->pos_map+100) - world->pos_m[i][0]) <= 200) {
            world->pos_m[i][0] += 1;
            if (i > 1)
                world->mob[i][0].x = 155;
        }
        else {
            world->pos_m[i][0] += world->pos_m[i][1];
            if (world->pos_m[i][0] % 80 == 0) {
                if (i > 1)
                    world->mob[i][0].x += world->pos_m[i][1] * (-152);
                world->pos_m[i][1] *= -1;
            }
        }
    }
}

void anim_end(world_t *world)
{
    int init_x = 199 + MARIO_WIDTH;
    int init_y = 161;
    int i = 0;

    while (world->marioD.y < 161) {
        SDL_RenderClear(world->ecran);
        world->marioD.y++;
        SDL_RenderCopy(world->ecran, world->fond, &world->SrcR, &world->DestR);
        SDL_RenderCopy(world->ecran, world->mario, &world->marioE, &world->marioD);
        SDL_RenderPresent(world->ecran);
        SDL_Delay(50);
    }
    world->marioE.x = 47;
    world->marioE.y = 35;
    while (world->marioD.y < 177) {
        SDL_RenderClear(world->ecran);
        world->marioD.y = init_y + i*i - i*15 - 1;
        world->marioD.x = init_x + i*6;
        i++;
        if (world->marioD.y >= 177)
            world->marioD.y = 177;
        SDL_RenderCopy(world->ecran, world->fond, &world->SrcR, &world->DestR);
        SDL_RenderCopy(world->ecran, world->mario, &world->marioE, &world->marioD);
        SDL_RenderPresent(world->ecran);
        SDL_Delay(70);
    }
    world->marioE.x = 152;
    world->marioE.y = 77;
    SDL_RenderClear(world->ecran);
    SDL_RenderCopy(world->ecran, world->fond, &world->SrcR, &world->DestR);
    SDL_RenderCopy(world->ecran, world->mario, &world->marioE, &world->marioD);
    SDL_RenderPresent(world->ecran);
    SDL_Delay(1000);
    world->marioS.x = 3;
    world->marioS.y = 77;
    while (world->marioD.x < 368 + MARIO_WIDTH) {
        if (world->marioD.x + MARIO_WIDTH <= 360)
            world->marioD.x++;
        else {
            world->marioD.x++;
            world->marioS.w--;
            world->marioD.w--;
        }
        SDL_RenderClear(world->ecran);
        SDL_RenderCopy(world->ecran, world->fond, &world->SrcR, &world->DestR);
        SDL_RenderCopy(world->ecran, world->mario, &world->marioS, &world->marioD);
        SDL_RenderPresent(world->ecran);
        world->marioS.x = world->marioS.x + MARIO_WIDTH + 1;
        if (world->marioS.x > 65)
            world->marioS.x = 24;
        SDL_Delay(70);
    }
    SDL_RenderClear(world->ecran);
    SDL_RenderCopy(world->ecran, world->win, NULL, &world->DestR);
    SDL_RenderPresent(world->ecran);
    SDL_Delay(2000);
    world->terminer = true;
}

// Procédure qui gère le déplacement et les animations du personnage
void move(world_t *world)
{
    if (!world->ausol)
        world->vitesse_fond = 1;
    else
        world->vitesse_fond = 3;    
    if (world->moveR == true) {
        world->pos_map += world->vitesse_fond;
        world->SrcR.x = world->SrcR.x + world->vitesse_fond;
        world->calqueR.x = world->calqueR.x + world->vitesse_fond;
        world->marioS.x = world->marioS.x + MARIO_WIDTH + 1;
        if (world->marioS.x > 65)
            world->marioS.x = 24;
        if (world->SrcR.x > 2960) {
            world->SrcR.x = 2960;
            world->marioD.x += world->vitesse_fond;
            while (world->marioD.x >= 192 && world->marioD.y > 161)
                world->marioD.x--;
        }
        if (world->marioD.x >= 199)
            anim_end(world);
    }
    if (world->moveL == true) {
        world->pos_map -= world->vitesse_fond;
        world->SrcR.x = world->SrcR.x - world->vitesse_fond;
        world->calqueR.x = world->calqueR.x - world->vitesse_fond;
        if (world->marioS.x > 25 || world->marioS.x > 15)
            world->marioS.x = world->marioS.x + MARIO_WIDTH + 1;
        if (world->marioS.x > 128)
            world->marioS.x = 87;
        if(world->SrcR.x<0) {
        world->SrcR.x=world->SrcR.x+world->vitesse_fond;
        world->calqueR.x = world->calqueR.x + world->vitesse_fond;
        }
    }
}

// Procédure qui gère les événements du clavier
void make_event(SDL_Event evenements, world_t *world)
{
    switch(evenements.type) {   
        case SDL_QUIT:
            world->terminer = true; 
            break;
        case SDL_KEYUP:
            switch(evenements.key.keysym.sym)
            {
            case SDLK_RIGHT:
                world->moveR = false;
                world->marioS.x = 3;
                break;
            case SDLK_LEFT:
                world->moveL = false;
                world->marioS.x = 66;
                break;
            }
            break;
        case SDL_KEYDOWN:
            switch(evenements.key.keysym.sym)
            {
            case SDLK_RIGHT:
                world->moveR = true;
                world->moveL = false;
                break;
            case SDLK_LEFT:
                world->marioS.x = 87;
                world->moveR = false;
                world->moveL = true;
                break;
            case SDLK_SPACE:
                world->ausol = false;
                break;
            case SDLK_ESCAPE:
            case SDLK_q:
                world->terminer = true;
                break;
            break;
        }
    }
}

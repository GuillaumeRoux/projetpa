#include "fonctions_SDL.h"
#include "world.h"

// Fonction qui permet de charger une texture
SDL_Texture* charger_image (const char* nomfichier, SDL_Renderer* renderer)
{
    // Charger une image
    SDL_Surface* surface = SDL_LoadBMP(nomfichier) ;
    // Convertir la surface de l’image au format texture avant de l’appliquer
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer,surface) ;
    return texture;
}

// Fonction qui permet de charger une surface
SDL_Surface* charger_surface (const char* nomfichier)
{
    // Charger une image
    SDL_Surface* surface = SDL_LoadBMP(nomfichier) ;
    return surface;
}


// Procédure qui gère le saut du mario
void saut(world_t *world)
{
if (world->i < 60) {
        if (world->i % world->k == 0)
            world->marioD.y -= 3;
    } else {
        if (world->i % world->k == 0)
            world->marioD.y += 3;
    }
    world->i++;
    if (world->i % 10 == 0 && world->i != 60) {
        if (world->i <= 60) {
            world->k++;
        } else {
            world->k--;
        }
    }
    if (world->i == 117) {
        world->i = 0;
        world->k = 1;
        world->ausol = true;
    }
}

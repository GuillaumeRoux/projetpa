#include "import.h"

// Structure qui contient toutes les varibles utiles au jeu
typedef struct world_s {
    int pos_map;
    int **pos_m;
    int vitesse_fond;
    bool terminer;
    bool ausol;
    bool moveR;
    bool moveL;
    SDL_Texture* win;
    SDL_Renderer* ecran;
    SDL_Texture* gameover;
    SDL_Texture* mario;
    SDL_Texture* fond;
    SDL_Surface* calque;
    SDL_Texture* mobT; 
    SDL_Rect **mob;
    SDL_Rect calqueR;
    SDL_Rect calqueD;
    SDL_Rect SrcR;
    SDL_Rect DestR;
    SDL_Rect marioS;
    SDL_Rect marioD;
    SDL_Rect marioE;
    int i;
    int k;
    int gravite;
} world_t;

// Déclaration des procédures
void position_initial(world_t *);
void make_event(SDL_Event, world_t *);
void saut(world_t *);
void move(world_t *);
void collision(world_t *);
void check_collision(world_t *);
void position_mob(world_t *);
void pos_mob(world_t *);
void move_mob(world_t *);
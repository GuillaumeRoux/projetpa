#include "fonctions_SDL.h"
#include "import.h"
#include "world.h"

// Procédure qui vérifie si les 4 points du sprites du personnage est en collision avec un bloc

void check_collision(world_t *world)
{
    Uint8 r, g, b, a1, a2, a3, a4;
    Uint32 coul1, coul2, coul3, coul4;

    //Haut gauche
    coul1 = *((Uint8 *)world->calque->pixels + (world->marioD.y + DIFF_COLLISION) * world->calque->pitch + (world->SrcR.x + POS_X) * world->calque->format->BytesPerPixel);
    SDL_GetRGBA(coul1, world->calque->format, &r, &g, &b, &a1);
    
    //Haut droite
    coul2 = *((Uint8 *)world->calque->pixels + (world->marioD.y + DIFF_COLLISION) * world->calque->pitch + (world->SrcR.x + POS_X + MARIO_WIDTH) * world->calque->format->BytesPerPixel);
    SDL_GetRGBA(coul2, world->calque->format, &r, &g, &b, &a2);
    
    //Bas droite
    coul3 = *((Uint8 *)world->calque->pixels + (world->marioD.y+MARIO_HEIGHT) * world->calque->pitch + (world->SrcR.x + POS_X + MARIO_WIDTH) * world->calque->format->BytesPerPixel);
    SDL_GetRGBA(coul3, world->calque->format, &r, &g, &b, &a3);
    
    //Bas gauche
    coul4 = *((Uint8 *)world->calque->pixels + (world->marioD.y+MARIO_HEIGHT) * world->calque->pitch + (world->SrcR.x + POS_X) * world->calque->format->BytesPerPixel);
    SDL_GetRGBA(coul4, world->calque->format, &r, &g, &b, &a4);

    //Vérification de la collision pour le point en haut à gauche
    if (a1 == ALPHA) 
    {
        world->pos_map += world->vitesse_fond;
        world->SrcR.x = world->SrcR.x + world->vitesse_fond;
        world->calqueR.x = world->calqueR.x + world->vitesse_fond;
        while (!world->ausol && a1== ALPHA) 
        {
            world->marioD.y += 1;
            coul1 = *((Uint8 *)world->calque->pixels + (world->marioD.y + DIFF_COLLISION)* world->calque->pitch + (world->SrcR.x + POS_X) * world->calque->format->BytesPerPixel);
            SDL_GetRGBA(coul1, world->calque->format, &r, &g, &b, &a1);
        }
    }
    
    //Vérification de la collision pour le point en haut à droite
    if (a2 == ALPHA) {
        world->pos_map -= world->vitesse_fond;
        world->SrcR.x = world->SrcR.x - world->vitesse_fond;
        world->calqueR.x = world->calqueR.x - world->vitesse_fond;
        while (!world->ausol && a2 == ALPHA) 
        {
            world->marioD.y += 1;
            coul2 = *((Uint8 *)world->calque->pixels + (world->marioD.y + DIFF_COLLISION) * world->calque->pitch + (world->SrcR.x + POS_X + MARIO_WIDTH) * world->calque->format->BytesPerPixel);
            SDL_GetRGBA(coul2, world->calque->format, &r, &g, &b, &a2);
        }
    }
    
    //Vérification de la collision pour le point en bas à droite && bas à gauche
    while (a3 == ALPHA || a4 == ALPHA) 
    {
        world->marioD.y -= 1;
        coul3 = *((Uint8 *)world->calque->pixels + (world->marioD.y+MARIO_HEIGHT) * world->calque->pitch + (world->SrcR.x + POS_X + MARIO_WIDTH) * world->calque->format->BytesPerPixel);
        SDL_GetRGBA(coul3, world->calque->format, &r, &g, &b, &a3);
        coul4 = *((Uint8 *)world->calque->pixels + (world->marioD.y+MARIO_HEIGHT) * world->calque->pitch + (world->SrcR.x + POS_X) * world->calque->format->BytesPerPixel);
        SDL_GetRGBA(coul4, world->calque->format, &r, &g, &b, &a4);
        world->ausol = true;
        world->i = 0;
        world->k =1;
    }
}


// Procédure qui gère la fin du jeu quand le mario meurt
void collision(world_t *world)
{    
    if (world->marioD.y >= 250) 
    {
        SDL_RenderCopy(world->ecran, world->gameover, NULL, &world->DestR);            
        SDL_RenderPresent(world->ecran);
        SDL_Delay(2000);
        world->terminer = true;
    }
}
